import { SocketEvents } from '../../types/socket-event';
import { UserAction, UserActionEnum } from '../actions/UserAction';
import { UserType } from '../types/UserType';

const initialState: UserType = {
  id: '',
  name: '',
  profilePicture: '',
  isOnline: true,
};

const onAssignUserId = (state: UserType, action: UserAction): UserType => {
  const { payload } = action;
  return { ...state, id: payload as string };
};

const onSetUser = (state: UserType, action: UserAction): UserType => {
  const { payload } = action;
  return { ...state, ...(payload as UserType) };
};

const onSetName = (state: UserType, action: UserAction): UserType => {
  const { payload } = action;
  return { ...state, name: payload as string };
};

export default function (state: UserType = initialState, action: UserAction) {
  switch (action.type) {
    case SocketEvents.ASSIGN_USER_ID:
      return onAssignUserId(state, action);
    case SocketEvents.ASSIGN_GUEST_ID:
      return onAssignUserId(state, action);
    case UserActionEnum.SET_USER:
      return onSetUser(state, action);
    case UserActionEnum.SET_NAME:
      return onSetName(state, action);
  }
  return state;
}
