import { ContactStatusType } from "../types/ContactType";
import { ContactAction, ContactActionEnum } from '../actions/ContactAction';

const initialState: ContactStatusType = {
  contactArrived: false,
  lastCursor: null,
  hasNext: false,
};

const onContactArrived = (state: ContactStatusType, action: ContactAction): ContactStatusType => {
  const { payload } = action;
  const { lastCursor, hasNext } = payload as {
    lastCursor: number | string | null;
    hasNext: boolean;
  };
  const newState : ContactStatusType = { ...state, contactArrived: true, lastCursor: lastCursor, hasNext: hasNext };
  return newState;
};

const onGetContact = (state: ContactStatusType): ContactStatusType => {
  return { ...state, contactArrived: false}
}

export default function (state: ContactStatusType = initialState, action: ContactAction) {
  switch (action.type) {
    case ContactActionEnum.CONTACT_ARRIVED:
      return onContactArrived(state, action);
    case ContactActionEnum.CONTACT_INIT:
      return onGetContact(state);
  }
  return state;
}
