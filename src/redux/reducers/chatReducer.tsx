import { getChatRoomIdxByName } from '../../helpers/room';
import { SocketEvents } from '../../types/socket-event';
import { ChatAction, ChatActionEnum } from '../actions/ChatAction';
import {
  ChatRoomDataType,
  ChatRoomReadyType,
  ChatRoomType,
  MessageConnectionType,
  MessageType,
} from '../types/ChatType';
import { UserType } from '../types/UserType';

const initialState: ChatRoomType[] = [];

const onInitChat = (
  state: ChatRoomType[],
  payload: unknown
): ChatRoomType[] => {
  const { otherPartyId } = payload as { otherPartyId: string };
  return [...state, { loading: true, otherPartyId }];
};

const onJoinChat = (
  state: ChatRoomType[],
  payload: unknown
): ChatRoomType[] => {
  const { roomName, participant } = payload as {
    roomName: string;
    participant: UserType;
  };
  const oldRoomIdx = getChatRoomIdxByName(state, roomName);
  if (oldRoomIdx === -1) {
    return state;
  }
  const oldRoom = state[oldRoomIdx] as ChatRoomReadyType;
  let oldParticipantIdx;
  oldParticipantIdx = oldRoom.participants.findIndex(
    ({ id }) => id === participant.id
  );

  if (oldParticipantIdx === -1) {
    return state;
  }
  const oldParticipant = oldRoom.participants[oldParticipantIdx];
  const newParticipant = {
    ...oldParticipant,
    isOnline: participant.isOnline || false,
  };
  const newParticipants = [...oldRoom.participants];
  newParticipants[oldParticipantIdx] = newParticipant;
  const newRoom: ChatRoomType = { ...oldRoom, participants: newParticipants };
  const newState = [...state];
  newState[oldRoomIdx] = newRoom;
  return newState;
};

const onSetRoomData = (
  state: ChatRoomType[],
  payload: unknown
): ChatRoomType[] => {
  const {
    roomName,
    participants,
    roomData,
    messageConnection,
    unreadMessageCount,
  } = payload as {
    roomName: string;
    participants: UserType[];
    roomData: ChatRoomDataType;
    messageConnection: MessageConnectionType;
    unreadMessageCount: number;
  };
  let oldRoomIdx = getChatRoomIdxByName(state, roomName);
  if (oldRoomIdx === -1) {
    oldRoomIdx = state.findIndex(({ loading }) => loading);
  }
  if (oldRoomIdx === -1) {
    const newRoom: ChatRoomType = {
      loading: false,
      name: roomName,
      participants,
      roomData,
      messageConnection,
      unreadMessageCount,
      active: false,
    };
    return [...state, newRoom];
  }
  const oldRoom = state[oldRoomIdx] as ChatRoomReadyType;
  const newRoom: ChatRoomReadyType = {
    ...oldRoom,
    loading: false,
    name: roomName,
    roomData,
    participants,
    messageConnection,
    unreadMessageCount,
  };
  const newState = [...state];
  newState[oldRoomIdx] = newRoom;
  return newState;
};

const onNewMessage = (
  state: ChatRoomType[],
  payload: unknown
): ChatRoomType[] => {
  const { roomId } = payload as MessageType;
  const roomName = `room:${roomId}`;
  const oldRoomIdx = getChatRoomIdxByName(state, roomName);
  if (oldRoomIdx === -1) {
    return state;
  }
  const oldRoom = state[oldRoomIdx] as ChatRoomReadyType;
  const newRoom: ChatRoomType = {
    ...oldRoom,
    messageConnection: {
      ...oldRoom.messageConnection,
      edges: [payload as MessageType, ...oldRoom.messageConnection.edges],
    },
    unreadMessageCount: oldRoom.active
      ? oldRoom.unreadMessageCount
      : oldRoom.unreadMessageCount + 1,
    newMsgArrived: true
  };

  const newState = [...state];
  newState[oldRoomIdx] = newRoom;

  return newState;
};

const onSetActiveRoom = (
  state: ChatRoomType[],
  payload: unknown
): ChatRoomType[] => {
  const { roomName } = payload as { roomName: string };
  const oldRoomIdx = getChatRoomIdxByName(state, roomName);
  if (oldRoomIdx === -1) {
    return state;
  }
  return state.map((chat) => {
    if (chat.loading) {
      return chat;
    }
    if (chat.name === roomName) {
      return { ...chat, active: true, unreadMessageCount: 0 };
    }
    if (chat.active) {
      return { ...chat, active: false };
    }
    return chat;
  });
};

const onUserLeft = (
  state: ChatRoomType[],
  payload: unknown
): ChatRoomType[] => {
  const { roomName, participantId } = payload as {
    roomName: string;
    participantId: string;
  };
  const oldRoomIdx = getChatRoomIdxByName(state, roomName);
  if (oldRoomIdx === -1) {
    return state;
  }
  const oldRoom = state[oldRoomIdx] as ChatRoomReadyType;
  const oldParticipantIdx = oldRoom.participants.findIndex(
    ({ id }) => id === participantId
  );
  if (oldParticipantIdx === -1) {
    return state;
  }
  const oldParticipant = oldRoom.participants[oldParticipantIdx];
  const newParticipant = { ...oldParticipant, isOnline: false };
  const newParticipants = [...oldRoom.participants];
  newParticipants[oldParticipantIdx] = newParticipant;
  const newRoom: ChatRoomType = { ...oldRoom, participants: newParticipants };
  const newState = [...state];
  newState[oldRoomIdx] = newRoom;
  return newState;
};

const onSetMessageConnection = (state: ChatRoomType[], payload: unknown) => {
  const { roomName, connection } = payload as {
    roomName: string;
    connection: MessageConnectionType;
  };
  const oldRoomIdx = getChatRoomIdxByName(state, roomName);
  const oldRoom = state[oldRoomIdx] as ChatRoomReadyType;
  const newRoom = {
    ...oldRoom,
    messageConnection: {
      ...oldRoom.messageConnection,
      edges: [...oldRoom.messageConnection.edges, ...connection.edges],
      lastCursor: connection.lastCursor,
      hasNext: connection.hasNext,
    },
  };
  const newState = [...state];
  newState[oldRoomIdx] = newRoom;
  return newState;
};

const onClose = (state: ChatRoomType[], payload: unknown) => {
  const { index } = payload as { index: number };
  const room = state[index];
  if (!room) {
    return state;
  }
  const newState = [...state];
  newState.splice(index, 1);
  return newState;
};

const onSetContacts = (
  state: ChatRoomType[],
  payload: unknown
): ChatRoomType[] => {
  const contacts = payload as {
    roomName: string;
    participants: UserType[];
    roomData: ChatRoomDataType;
    messageConnection: MessageConnectionType;
    unreadMessageCount: number;
  }[];

  // let contactList: ChatRoomType[] = [];
  contacts.forEach((contact) => {
    const existing = getChatRoomIdxByName(state, contact.roomName);
    
    if (existing < 0) {
      state.push({
        loading: false,
        active: false,
        name: contact.roomName,
        roomData: contact.roomData,
        participants: contact.participants,
        messageConnection: contact.messageConnection,
        unreadMessageCount: contact.unreadMessageCount,
      });
    }
  });

  return state;
};
const onClearNewArrived = (
  state: ChatRoomType[],
  payload: unknown
): ChatRoomType[] => {
  const contact = payload as ChatRoomReadyType;
  const oldRoomIdx = getChatRoomIdxByName(state, contact.name);
  const oldRoom = state[oldRoomIdx] as ChatRoomReadyType;
  const newRoom = {
    ...oldRoom,
    newMsgArrived: false
  };
  const newState = [...state];
  newState[oldRoomIdx] = newRoom;
  return newState;
}
export default function (
  state: ChatRoomType[] = initialState,
  action: ChatAction
) {
  const { payload } = action;
  switch (action.type) {
    case SocketEvents.INIT_CHAT:
      return onInitChat(state, payload);
    case SocketEvents.JOIN_CHAT:
      return onJoinChat(state, payload);
    case SocketEvents.SET_ROOM_DATA:
      return onSetRoomData(state, payload);
    case SocketEvents.SET_ACTIVE_ROOM:
      return onSetActiveRoom(state, payload);
    case SocketEvents.NEW_MESSAGE:
      return onNewMessage(state, payload);
    case SocketEvents.USER_LEFT:
      return onUserLeft(state, payload);
    case SocketEvents.SET_MESSAGE_CONNECTION:
      return onSetMessageConnection(state, payload);
    case ChatActionEnum.CLOSE:
      return onClose(state, payload);
    case ChatActionEnum.CLEAR_NEW_ARRIVED:
      return onClearNewArrived(state, payload);
    case SocketEvents.SET_CONTACTS:
      return onSetContacts(state, payload);
  }
  return state;
}
