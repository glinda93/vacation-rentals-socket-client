import { combineReducers } from 'redux';
import chatReducer from './chatReducer';
import userReducer from './userReducer';
import contactReducer from './contactReducer';

const rootReducer = combineReducers({
  chat: chatReducer,
  user: userReducer,
  contact: contactReducer
});

export default rootReducer;
