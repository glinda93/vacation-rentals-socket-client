import { SocketEvents } from '../../types/socket-event';

export enum UserActionEnum {
  SET_USER = 'SET_USER',
  SET_NAME = 'SET_NAME',
}

type UserActionType =
  | SocketEvents.ASSIGN_GUEST_ID
  | SocketEvents.ASSIGN_USER_ID
  | UserActionEnum.SET_USER
  | UserActionEnum.SET_NAME;

export type UserAction = {
  type: UserActionType;
  payload: unknown;
};
