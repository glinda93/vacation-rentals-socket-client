export enum DateActionType {
  UPDATE = 'UPDATE',
}

export type DateAction =  {
    type: DateActionType
}
