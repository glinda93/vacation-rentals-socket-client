import { SocketEvents } from '../../types/socket-event';

export enum ChatActionEnum {
  START = 'start',
  CLOSE = 'close',
  CLEAR_NEW_ARRIVED = "clear_new_arrived"
}

type ChatActionType =
  | SocketEvents.INIT_CHAT
  | SocketEvents.JOIN_CHAT
  | SocketEvents.SET_ROOM_DATA
  | SocketEvents.NEW_MESSAGE
  | SocketEvents.USER_LEFT
  | SocketEvents.SET_ACTIVE_ROOM
  | SocketEvents.SET_MESSAGE_CONNECTION
  | ChatActionEnum.CLOSE
  | ChatActionEnum.CLEAR_NEW_ARRIVED
  | SocketEvents.SET_CONTACTS;

export type ChatAction = {
  type: ChatActionType;
  payload: unknown;
};
