export enum ContactActionEnum {
  CONTACT_ARRIVED = 'contact_arrived',
  CONTACT_INIT = 'contact_init'
}

type ContactActionType = ContactActionEnum.CONTACT_ARRIVED | ContactActionEnum.CONTACT_INIT;

export type ContactAction = {
  type: ContactActionType;
  payload: unknown;
};
