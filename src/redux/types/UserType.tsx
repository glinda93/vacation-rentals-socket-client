export type UserType = {
  id: string;
  name: string;
  profilePicture: string;
  isOnline?: boolean;
};
