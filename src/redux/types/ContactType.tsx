
export type ContactStatusType = {
  contactArrived: boolean;
  lastCursor: number | string | null;
  hasNext: boolean;
};

