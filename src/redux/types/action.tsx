export type Action<T> = {
  type: T;
  payload: unknown;
};
