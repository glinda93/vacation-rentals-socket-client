import { UserType } from './UserType';

export type MessageType = {
  id: number | string;
  roomId: number;
  userId: string;
  message: string;
  createdAt: string;
  updatedAt: string;
};

export type MessageConnectionType = {
  edges: MessageType[];
  lastCursor: number | string | null;
  hasNext: boolean;
};

export type ChatRoomDataType = {
  id: number | string;
  userId: string;
  name: string;
  lastReadtAt: string | null;
  createdAt: string;
  updatedAt: string;
};

export type ChatRoomLoadingType = {
  loading: true;
  otherPartyId: string;
};

export type ChatRoomReadyType = {
  loading: false;
  name: string;
  participants: UserType[];
  active: boolean;
  messageConnection: MessageConnectionType;
  unreadMessageCount: number;
  roomData: ChatRoomDataType;
  newMsgArrived?: boolean
};

export type ChatRoomType = ChatRoomLoadingType | ChatRoomReadyType;
