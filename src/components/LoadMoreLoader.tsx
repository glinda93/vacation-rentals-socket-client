import React from 'react';
import { makeStyles } from '@material-ui/core';
import Badge from '@material-ui/core/Badge';

const useStyles = makeStyles(() => ({
  container: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'white',
    zIndex: 10,
    padding: 20,
    top: 0,
  },
  loader: {},
}));

const Loader = ({ title }: { title?: string }) => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <Badge
        className={classes.loader}
        color="primary"
        badgeContent={title || 'Loading...'}
      />
    </div>
  );
};

export default Loader;
