import React, { forwardRef, useImperativeHandle } from 'react';
import { useSelector } from 'react-redux';
import useChat from '../hooks/useChat';
import { GlobalStateType } from '../types/global';
import Chatbox from './Chatbox';
import ChatBoard from './ChatBoard';

export enum ComponentType {
  WIDGET = 'widget',
  DASHBOARD = 'dashboard',
}

const ChatComponent = forwardRef(function ChatComponentDefinition(
  {
    layout,
  }: {
    layout: ComponentType | string | undefined;
  },
  ref
) {
  const chats = useSelector((state: GlobalStateType) => state.chat);
  const { start, lastRoomNames, getRoomData } = useChat(layout === 'widget');

  useImperativeHandle(ref, () => ({
    startChat(participantId: string) {
      start(participantId);
    },
    startLastChats() {
      if (lastRoomNames.length > 0) {
        lastRoomNames.forEach((roomName) => {
          getRoomData(roomName);
        });
      }
    },
  }));

  if (layout === ComponentType.WIDGET) {
    return (
      <React.Fragment>
        {chats.map((chat, index) => (
          <Chatbox index={index} key={`chat_box_${index}`} chat={chat} />
        ))}
      </React.Fragment>
    );
  }
  if (layout === ComponentType.DASHBOARD) {
    return (
      <React.Fragment>
        <ChatBoard/>
      </React.Fragment>
    );
  }
  return null;
});

export default ChatComponent;
