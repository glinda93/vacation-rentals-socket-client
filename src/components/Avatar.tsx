import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import { ParticipantType } from '../types/participant';
import { DynamicObjectType } from '../types/global';

type UserAvatarPropsType = {
    user: ParticipantType,
} & DynamicObjectType;

const UserAvatar = ({ user, ...rest }: UserAvatarPropsType) => (
  <Avatar alt={user.name} src={user.profilePicture} {...rest}>
    {user.name.substring(0, 1).toUpperCase()}
  </Avatar>
);

export default UserAvatar;
