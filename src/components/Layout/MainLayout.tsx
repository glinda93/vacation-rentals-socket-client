import React from 'react';
import { Grid, makeStyles } from '@material-ui/core';
// import { relative } from 'path';

const useStyles = makeStyles({
  widgetContainer: {
    width: '100%',
    height: '100%',
    zIndex: 9999,
    background: 'transparent',
    pointerEvents: 'none',
    position: 'fixed',
    top: 0,
    left: 0,
    alignContent: 'end',
    alignItems: 'flex-end',
    flexWrap: 'wrap-reverse',
    justifyContent: 'flex-end',
    overflowY: 'scroll',
  },
  dashboardContainer: {
    width: '100%',
    height: '100vh',
    position: "relative",
  },
});

export const MainLayout = ({
  children,
  layout,
}: {
  children: JSX.Element[] | JSX.Element;
  layout: 'widget' | 'dashboard';
}) => {
  const classes = useStyles();
  const classKey = layout + 'Container';
  return (
    <Grid
      container
      className={classes[classKey as 'widgetContainer' | 'dashboardContainer']}
    >
      {children}
    </Grid>
  );
};
