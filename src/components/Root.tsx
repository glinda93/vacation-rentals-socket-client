import React, { forwardRef, useImperativeHandle, useRef } from 'react';
import { Provider as StoreProvider } from 'react-redux';
import store from '../redux/store';
import useSocket, { SocketContext } from '../hooks/useSocket';
import useTick, { TickContext } from '../hooks/useTick';
import SocketConsumer from './SocketConsumer';
import UserConsumer from './UserConsumer';
import { MainLayout } from './Layout/MainLayout';
import ChatComponent, { ComponentType } from './ChatComponent';
import { ConfigContext } from '../hooks/ConfigContext';

type RootPropsType = {
  socketUrl: string;
  guestStorageKey: string;
  tokenCookieKey: string;
  layout?: string;
};

interface ChatInterface {
  startChat: (participantId: string) => void;
  startLastChats: () => void;
}

const Root = forwardRef(function RootDefinition(
  { socketUrl, guestStorageKey, tokenCookieKey, layout }: RootPropsType,
  ref
) {
  const socket = useSocket(socketUrl, guestStorageKey, tokenCookieKey);
  const chatRef = useRef<ChatInterface>();
  useImperativeHandle(ref, () => ({
    startChat: (participantId: string) => {
      if (chatRef && chatRef.current && layout === ComponentType.WIDGET) {
        chatRef.current.startChat(participantId);
      }
    },
    startLastChats: () => {
      if (chatRef && chatRef.current && layout === ComponentType.WIDGET) {
        chatRef.current.startLastChats();
      }
    },
  }));
  const tick = useTick();
  return (
    <ConfigContext.Provider
      value={{
        socketUrl,
        guestStorageKey,
        tokenCookieKey,
        layout: layout as string,
      }}
    >
      <SocketContext.Provider value={socket}>
        <StoreProvider store={store}>
          {/* Subscribe to socket events and dispatch redux */}
          <SocketConsumer />
          {/* Persist guest Id to local storage */}
          <UserConsumer localStorageKey={guestStorageKey} />
          <MainLayout layout={(layout || 'widget') as ComponentType}>
            <TickContext.Provider value={tick}>
              <ChatComponent ref={chatRef} layout={layout} />
            </TickContext.Provider>
          </MainLayout>
        </StoreProvider>
      </SocketContext.Provider>
    </ConfigContext.Provider>
  );
});

export default Root;
