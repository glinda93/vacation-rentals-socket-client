import React, { useCallback, useContext, useState } from 'react';
import { makeStyles, Typography } from '@material-ui/core';
import { ChatRoomType } from '../../redux/types/ChatType';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Loader from './Loader';
import { isAnonymous } from '../../helpers/user';
import UsernameForm from './UsernameForm';
import ChatInput from './ChatInput';
import { UserType } from '../../redux/types/UserType';
import InfiniteScroll from 'react-infinite-scroll-component';
import MessageThread from './MessageThread';
import LoadMoreLoader from '../LoadMoreLoader';
import { ConfigContext } from '../../hooks/ConfigContext';
import * as Cookie from '../../hooks/useCookie';

const useStyles = makeStyles(() => ({
  container: {
    height: 'calc(50vh - 50px)',
    minHeight: '370px',
    padding: '5px!important',
  },
  loaderContainer: {
    display: 'flex',
    alignSelf: 'center',
    flexDirection: 'column',
  },
  loader: {
    padding: '15px',
  },
  contentContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
  messagesContainer: {
    display: 'flex',
    flexDirection: 'column-reverse',
    flex: 1,
    overflow: 'auto',
    scrollbarWidth: 'thin',
    '&::-webkit-scrollbar': {
      width: '0.4em',
    },

    '&::-webkit-scrollbar-track': {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
      webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,.1)',
    },
  },
  infiniteScroll: {
    display: 'flex',
    flexDirection: 'column-reverse',
    flex: 1,
  },
}));

const ChatboxBody = ({
  chat,
  collapsed,
  user,
  onSetUsername,
  onSendMessage,
  onLoadMore,
  index,
}: {
  chat: ChatRoomType;
  collapsed: boolean;
  user: UserType;
  onSetUsername: (name: string) => void;
  onSendMessage: (message: string) => void;
  onLoadMore: () => any;
  index: number;
}) => {
  const classes = useStyles();
  const [input, setInput] = useState('');
  const handleSubmit = useCallback(() => {
    onSendMessage(input);
    setInput('');
  }, [onSendMessage, input]);
  const { tokenCookieKey } = useContext(ConfigContext);
  return (
    <Collapse in={!collapsed} timeout="auto" unmountOnExit>
      <CardContent className={classes.container}>
        {!Cookie.getItem(tokenCookieKey) && isAnonymous(user) ? (
          <UsernameForm onSubmit={onSetUsername} />
        ) : (
          <React.Fragment>
            {chat.loading ? (
              <div className={classes.loaderContainer}>
                <Typography variant="body2">Just a moment...</Typography>
                <Loader containerProps={{ className: classes.loader }} />
              </div>
            ) : (
              <div className={classes.contentContainer}>
                <div
                  className={classes.messagesContainer}
                  id={`chat-messages-${index}`}
                >
                  {chat.messageConnection.edges.length === 0 ? (
                    <Typography
                      align="center"
                      variant="caption"
                      display="block"
                      gutterBottom
                    >
                      There are no messages
                    </Typography>
                  ) : (
                    <InfiniteScroll
                      dataLength={chat.messageConnection.edges.length}
                      next={onLoadMore}
                      hasMore={chat.messageConnection.hasNext}
                      loader={<LoadMoreLoader />}
                      inverse={true}
                      className={classes.infiniteScroll}
                      scrollableTarget={`chat-messages-${index}`}
                    >
                      {chat.messageConnection.edges.map((edge) => (
                        <MessageThread
                          key={`chat-thread-${edge.id}`}
                          participants={chat.participants}
                          user={user}
                          thread={edge}
                        />
                      ))}
                    </InfiniteScroll>
                  )}
                </div>
                <ChatInput
                  value={input}
                  onChange={(val: string) => setInput(val)}
                  onSubmit={handleSubmit}
                />
              </div>
            )}
          </React.Fragment>
        )}
      </CardContent>
    </Collapse>
  );
};

export default ChatboxBody;
