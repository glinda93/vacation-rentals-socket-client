import React from 'react';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { MessageTypeEnum } from '../../helpers/chat';
import { MessageType } from '../../redux/types/ChatType';
import { UserType } from '../../redux/types/UserType';
import { ParticipantType } from '../../types/participant';
import useDuration from '../../hooks/useDuration';
import Avatar from '../Avatar';

type MessageThreadPropsType = {
  participants: ParticipantType[];
  user: UserType;
  thread: MessageType;
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column-reverse',
    flex: 1,
  },
  paper: {
    padding: '10px',
    backgroundColor: theme.palette.background.default,
    maxWidth: '80%',
    flex: 0,
    marginBottom: '5px',
    width: 'fit-content',
    lineBreak: 'anywhere',
    textAlign: 'left',
  },
  sent: {
    marginLeft: 'auto',
  },
  received: {
    backgroundColor: theme.palette.info.light,
    marginRight: 'auto',
    color: 'white',
  },
  sentTime: {
    display: 'block',
    textAlign: 'right',
    color: theme.palette.text.secondary,
    marginBottom: '10px',
  },
  receivedMessageContainer: {
    display: 'flex',
  },
  avatarContainer: {
    display: 'flex',
    flex: 0,
  },
  receivedContentContainer: {
    display: 'flex',
    flexDirection: 'column-reverse',
    paddingLeft: 10,
    flex: 1,
  },
  receivedTime: {
    display: 'block',
    textAlign: 'left',
    color: theme.palette.text.secondary,
    marginBottom: '10px',
  },
}));

const MessageThread = ({
  participants,
  user,
  thread,
}: MessageThreadPropsType) => {
  const messageTime = useDuration(new Date(thread.createdAt));
  const classes = useStyles();
  const author = participants.find(({ id }) => id === thread.userId);
  if (!author) {
    return null;
  }
  const type =
    author.id === user.id ? MessageTypeEnum.SENT : MessageTypeEnum.RECEIVED;
  return (
    <div className={classes.container}>
      {type === MessageTypeEnum.SENT && (
        <React.Fragment>
          <Typography variant="caption" className={classes.sentTime}>
            {messageTime}
          </Typography>
          <Paper className={`${classes.paper} ${classes.sent}`}>
            {thread.message}
          </Paper>
        </React.Fragment>
      )}
      {type === MessageTypeEnum.RECEIVED && (
        <div className={classes.receivedMessageContainer}>
          <div className={classes.avatarContainer}>
            <Avatar user={author} />
          </div>
          <div className={classes.receivedContentContainer}>
            <Typography variant="caption" className={classes.receivedTime}>
              {author.name} {messageTime}
            </Typography>
            <Paper className={`${classes.paper} ${classes.received}`}>
              {thread.message}
            </Paper>
          </div>
        </div>
      )}
    </div>
  );
};

export default MessageThread;
