import React, { useState } from 'react';
import { Button, Input, Typography, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  container: {
    padding: '15px',
  },
  mb: {
    marginBottom: '7px',
  },
});

const isValidName = (name: string): boolean => {
  return !!name && name.length > 2 && name.length < 25;
};

const UsernameForm = ({ onSubmit }: { onSubmit: (name: string) => void }) => {
  const classes = useStyles();
  const [name, setName] = useState('');
  return (
    <div className={classes.container}>
      <Typography variant="body1">
        Hello, there! Seems like you're new to Vacation.Rentals. How should we
        call you?
      </Typography>
      <Input
        className={classes.mb}
        fullWidth
        inputProps={{
          minLength: 3,
        }}
        placeholder="Input your name here"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <Button
        variant="contained"
        disabled={!isValidName(name)}
        color="primary"
        onClick={() => onSubmit(name)}
      >
        Next
      </Button>
    </div>
  );
};

export default UsernameForm;
