import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import CardHeader from '@material-ui/core/CardHeader';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CloseIcon from '@material-ui/icons/Close';
import { ChatRoomType } from '../../redux/types/ChatType';

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.success.light,
    color: 'white',
    padding: '10px!important',
    textAlign: 'left',
  },
  containerActive: {
    backgroundColor: theme.palette.success.main,
  },
  action: {
    marginTop: '8px',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

const getKey = (chat: ChatRoomType) => {
  if (chat.loading) {
    return 'room:loading';
  }
  return chat.name;
};

const ChatboxHeader = ({
  chat,
  collapsed,
  onToggleCollapse,
  onClose,
}: {
  chat: ChatRoomType;
  collapsed: boolean;
  onToggleCollapse: () => void;
  onClose: () => void;
}) => {
  const classes = useStyles();
  return (
    <CardHeader
      className={`${classes.container} ${
        !chat.loading && chat.active ? classes.containerActive : ''
      }`}
      action={[
        <IconButton
          className={`${clsx(classes.expand, {
            [classes.expandOpen]: collapsed,
          })} ${classes.action}`}
          aria-expanded={collapsed}
          aria-label="toggle"
          key={`chatbox-collapse-btn-${getKey(chat)}`}
          onClick={onToggleCollapse}
          size="small"
        >
          <ExpandMoreIcon />
        </IconButton>,
        <IconButton
          className={classes.action}
          key={`chatbox-close-btn-${getKey(chat)}`}
          onClick={onClose}
          size="small"
        >
          <CloseIcon />
        </IconButton>,
      ]}
      title={
        <React.Fragment>
          {!chat.loading && (
            <React.Fragment>
              {chat.unreadMessageCount > 0 ? (
                <Badge badgeContent={chat.unreadMessageCount} color="secondary">
                  <Typography variant="button">{chat.roomData.name}</Typography>
                </Badge>
              ) : (
                <Typography variant="button">{chat.roomData.name}</Typography>
              )}
            </React.Fragment>
          )}
        </React.Fragment>
      }
    />
  );
};

export default ChatboxHeader;
