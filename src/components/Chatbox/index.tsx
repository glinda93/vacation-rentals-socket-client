import React, { useState, useCallback, useContext, useEffect } from 'react';
import Card from '@material-ui/core/Card';
import { ChatRoomType } from '../../redux/types/ChatType';
import { makeStyles } from '@material-ui/core';
import Header from './ChatboxHeader';
import Body from './ChatboxBody';
import useChat from '../../hooks/useChat';
import { SocketContext } from '../../hooks/useSocket';
import { SocketEvents } from '../../types/socket-event';
import { useDispatch, useSelector } from 'react-redux';
import { GlobalStateType } from '../../types/global';
import { UserActionEnum } from '../../redux/actions/UserAction';
import useAudio from '../../hooks/useAudio';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flex: '0 0 20%',
    flexDirection: 'column',
    padding: 0,
    position: 'relative',
    margin: 0,
    [theme.breakpoints.only('xl')]: {
      flexBasis: '20%',
    },
    [theme.breakpoints.only('lg')]: {
      flexBasis: '20%',
    },
    [theme.breakpoints.only('md')]: {
      flexBasis: '25%',
    },
    [theme.breakpoints.only('sm')]: {
      flexBasis: '50%',
    },
    [theme.breakpoints.only('xs')]: {
      flexBasis: '100%',
    },
  },
  card: {
    flexBasis: '100%',
    pointerEvents: 'auto',
    margin: 10,
    fontSize: '0.875rem'
  },
}));

const Chatbox = ({ chat, index }: { chat: ChatRoomType; index: number }) => {
  const classes = useStyles();
  const [collapsed, setCollapsed] = useState(false);
  const socket = useContext(SocketContext);
  const user = useSelector((state: GlobalStateType) => state.user);
  const { play: playNotificationSound } = useAudio(
    'https://blueskyapi.vacation.rentals/message-notification.mp3'
  );
  const dispatch = useDispatch();
  const { close, setActive, sendMessage, getMessageConnection } = useChat(true);

  const handleClose = useCallback(() => {
    if (!chat.loading && socket) {
      socket.emit(SocketEvents.LEAVE_CHAT, chat.name);
    }
    close(index);
  }, [chat, socket, close, index]);

  const handleSetUsername = useCallback(
    (name: string) => {
      dispatch({
        type: UserActionEnum.SET_NAME,
        payload: name,
      });
      if (socket) {
        socket.emit(SocketEvents.SET_USERNAME, { name });
      }
    },
    [dispatch, socket]
  );

  const handleSendMessage = useCallback(
    (message: string) => {
      if (chat.loading) {
        return;
      }
      sendMessage(chat.name, message);
    },
    [chat, sendMessage]
  );

  const handleCardClick = useCallback(() => {
    if (chat.loading) {
      return;
    }
    setActive(chat.name);
  }, [chat, setActive]);

  const handleLoadMore = useCallback(() => {
    if (chat.loading) {
      return;
    }
    const cursor = chat.messageConnection.lastCursor;
    getMessageConnection(chat.name, cursor);
  }, [chat, getMessageConnection]);

  useEffect(() => {
    if (!chat.loading && chat.unreadMessageCount > 0) {
      playNotificationSound();
    }
  }, [chat, playNotificationSound]);

  return (
    <div className={classes.container}>
      <Card className={classes.card} onClick={handleCardClick}>
        <Header
          chat={chat}
          collapsed={collapsed}
          onClose={handleClose}
          onToggleCollapse={() => setCollapsed(!collapsed)}
        />
        <Body
          chat={chat}
          index={index}
          user={user}
          collapsed={collapsed}
          onSetUsername={handleSetUsername}
          onSendMessage={handleSendMessage}
          onLoadMore={handleLoadMore}
        />
      </Card>
    </div>
  );
};

export default Chatbox;
