import React, { useCallback, useState } from 'react';
import { makeStyles } from '@material-ui/core';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send';
import EmojiIcon from '@material-ui/icons/EmojiEmotions';
import Picker from 'emoji-picker-react';

const useStyles = makeStyles({
  input: {
    flex: 0,
    maxHeight: '96px',
    minHeight: '32px',
    '& .MuiInputBase-inputMultiline': {
      height: '24px !important',
      fontSize: '0.875rem',
    },
  },
  emojiContainer: {
    display: 'flex',
    maxWidth: '100%',
  },
});

const Buttons = ({
  onSubmit,
  onToggleEmoji,
}: {
  onSubmit: any;
  onToggleEmoji: any;
}) => (
  <InputAdornment position="end">
    <IconButton aria-label="Pick Emoji" onClick={onToggleEmoji}>
      <EmojiIcon />
    </IconButton>
    <IconButton aria-label="Send Message" onClick={onSubmit} edge="end">
      <SendIcon />
    </IconButton>
  </InputAdornment>
);

const ChatInput = ({
  value,
  onChange,
  onSubmit,
}: {
  value: string;
  onChange: Function;
  onSubmit: Function;
}) => {
  const classes = useStyles();
  const [showEmojiPicker, setShowEmojiPicker] = useState(false);
  const handleToggleEmoji = useCallback(() => {
    setShowEmojiPicker((oldState) => !oldState);
  }, []);
  const handleEmojiClick = useCallback(
    (_, { emoji }) => {
      setShowEmojiPicker(false);
      onChange(value + emoji);
    },
    [onChange, value]
  );
  const handleKeyDown = useCallback(
    (e) => {
      if (e.keyCode !== 13) {
        return true;
      }
      if (!e.shiftKey) {
        e.preventDefault();
        return onSubmit();
      }
      onChange(value + '\n');
    },
    [onSubmit, onChange, value]
  );
  return (
    <React.Fragment>
      {showEmojiPicker ? (
        <div className={classes.emojiContainer}>
          <Picker onEmojiClick={handleEmojiClick} />
        </div>
      ) : (
        <Input
          className={classes.input}
          multiline
          fullWidth
          placeholder="Send a message"
          value={value}
          onChange={(e) => onChange(e.target.value)}
          onKeyDown={handleKeyDown}
          endAdornment={
            <Buttons onSubmit={onSubmit} onToggleEmoji={handleToggleEmoji} />
          }
        />
      )}
    </React.Fragment>
  );
};

export default ChatInput;
