import React from 'react';
import { Box, CircularProgress } from '@material-ui/core';
import { DynamicObjectType } from '../../types/global';

const Loader = ({
  containerProps,
  loaderProps,
}: {
  containerProps?: DynamicObjectType;
  loaderProps?: DynamicObjectType;
}) => (
  <Box
    display="flex"
    justifyContent="center"
    alignItems="center"
    {...containerProps}
  >
    <CircularProgress {...loaderProps} />
  </Box>
);

export default Loader;
