import { useContext, useEffect } from 'react';
import { SocketContext } from '../hooks/useSocket';
import { useDispatch } from 'react-redux';
import { SocketEvents } from '../types/socket-event';
// import { ContactActionEnum } from '../redux/actions/ContactAction';
import useContact from "../hooks/useContact";

const SocketConsumer = () => {
  const socket = useContext(SocketContext);
  const dispatch = useDispatch();
  const { onSetContacts } = useContact();
  useEffect(() => {
    if (!socket) {
      return;
    }
    const handleSocketEvent = (event: SocketEvents, payload: any) => {
      if (event === SocketEvents.SET_CONTACTS) {
        const { contacts, lastCursor, hasNext } = payload;

        dispatch({
          type: event,
          payload: contacts,
        });
        onSetContacts(lastCursor, hasNext);
      } else {
        dispatch({
          type: event,
          payload,
        });
      }
    };
    /** @ts-ignore */
    socket.onAny(handleSocketEvent);
    return () => {
      /** @ts-ignore */
      socket.offAny(handleSocketEvent);
    };
  }, [socket, dispatch, onSetContacts]);
  return null;
};

export default SocketConsumer;
