import React, { useState, useCallback } from 'react';
import { makeStyles, Typography } from '@material-ui/core';
import { ChatRoomReadyType } from '../../redux/types/ChatType';
import { UserType } from '../../redux/types/UserType';
import MessageThread from '../Chatbox/MessageThread';
import InfiniteScroll from 'react-infinite-scroll-component';
import LoadMoreLoader from '../LoadMoreLoader';
import Loader from '../Chatbox/Loader';
import ChatInput from '../Chatbox/ChatInput';

const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
  loaderContainer: {
    display: 'flex',
    alignSelf: 'center',
    flexDirection: 'column',
  },
  loader: {
    padding: '15px',
  },
  contentContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    width: '100%',
    maxWidth: '1200px',
  },
  messagesContainer: {
    display: 'flex',
    flexDirection: 'column-reverse',
    flex: 1,
    overflow: 'auto',
    scrollbarWidth: 'thin',
    '&::-webkit-scrollbar': {
      width: '0.4em',
    },
    '&::-webkit-scrollbar-track': {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
      webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,.1)',
    },
  },
  infiniteScroll: {
    display: 'flex',
    flexDirection: 'column-reverse',
    flex: 1,
    paddingRight: '20px',
  },
  messageWrapper: {
    padding: '10px',
  },
  InputWrapper: {
    [theme.breakpoints.up('sm')]: {
      paddingRight: '20px',
    },
  },
  messageStarter: {
    marginTop: '20px',
    padding: '10px',
  },
}));

const ChatBoardBody = ({
  contact,
  user,
  onSendMessage,
  onLoadMore,
}: {
  contact: ChatRoomReadyType;
  user: UserType;
  onSendMessage: (message: string) => void;
  onLoadMore: () => any;
}) => { 
    
  const classes = useStyles();
  const [input, setInput] = useState('');
  const handleSubmit = useCallback(() => {
    onSendMessage(input);
    setInput('');
  }, [onSendMessage, input]);

    return (
      <React.Fragment>
        {contact.loading ? (
          <div className={classes.loaderContainer}>
            <Typography variant="body2">Just a moment...</Typography>
            <Loader containerProps={{ className: classes.loader }} />
          </div>
        ) : (
          <div className={classes.contentContainer}>
            <div className={classes.messagesContainer} id={`chat-messages`}>
              {contact.messageConnection.edges.length === 0 ? (
                <Typography
                  align="center"
                  variant="caption"
                  display="block"
                  gutterBottom
                >
                  There are no messages
                </Typography>
              ) : (
                <InfiniteScroll
                  dataLength={contact.messageConnection.edges.length}
                  next={onLoadMore}
                  hasMore={contact.messageConnection.hasNext}
                  loader={<LoadMoreLoader />}
                  inverse={true}
                  className={classes.infiniteScroll}
                  scrollableTarget={`chat-messages`}
                >
                  {contact.messageConnection.edges.map((edge) => (
                    <MessageThread
                      key={`chat-thread-${edge.id}`}
                      participants={contact.participants}
                      user={user}
                      thread={edge}
                    />
                  ))}
                  {!contact.messageConnection.hasNext && (
                    <div className={classes.messageStarter}>
                      This is the start of your chatting.
                    </div>
                  )}
                </InfiniteScroll>
              )}
            </div>
            <div className={classes.InputWrapper}>
              <ChatInput
                value={input}
                onChange={(val: string) => setInput(val)}
                onSubmit={handleSubmit}
              />
            </div>
          </div>
        )}
      </React.Fragment>
    );
};

export default ChatBoardBody;