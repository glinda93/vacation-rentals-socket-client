import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Contacts from './Contacts';
import { GlobalStateType } from '../../types/global';
import { ChatRoomReadyType } from '../../redux/types/ChatType';
import useChat from '../../hooks/useChat';
import ChatBoardBody from './ChatBoardBody';
import { getContactName } from '../../helpers/contact';
import EmptyBoard from './EmptyBoard';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => {
  return {
    root: {
      display: 'flex',
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    appBar: {
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: drawerWidth,
      position: 'absolute',
    },
    content: {
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
      },
      [theme.breakpoints.down('xs')]: {
        width: '100%',
      },
      // width: "100%",
      height: '100%',
      position: 'absolute',
      bottom: 0,
      padding: theme.spacing(3),
      paddingTop: `${theme.mixins.toolbar.minHeight}px`,
      display: 'flex',
      justifyContent: 'center',
    },
    emptyWrapper: {
      display: 'flex',
    },
  };
});

function ChatBoard(props: any) {
  const { window } = props;
  const theme = useTheme();

  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const container =
    window !== undefined ? () => window().document.body : undefined;
  const classes = useStyles();

  const contacts = useSelector((state: GlobalStateType) => state.chat);
  const { setActive, sendMessage, getMessageConnection } = useChat(false);

  const handleContactClick = useCallback(
    (contact: ChatRoomReadyType) => {
      if (contact.loading) {
        return;
      }
      setActive(contact.name);
    },
    [setActive]
  );

  const user = useSelector((state: GlobalStateType) => state.user);
  const activeRoom = contacts.filter(
    (contact) => (contact as ChatRoomReadyType).active
  )[0] as ChatRoomReadyType;

  const handleSendMessage = useCallback(
    (message: string) => {
      if (activeRoom && activeRoom.loading) {
        return;
      }
      sendMessage(activeRoom.name, message);
    },
    [activeRoom, sendMessage]
  );
  const handleLoadMore = useCallback(() => {
    if (activeRoom.loading) {
      return;
    }
    const cursor = activeRoom.messageConnection.lastCursor;
    getMessageConnection(activeRoom.name, cursor);
  }, [activeRoom, getMessageConnection]);

  const headerPlaceHolder = 'Build stronger relationships at vacation.rentals';
  // const emptySvg = require('./empty.svg') as string;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            {activeRoom ? getContactName(activeRoom) : headerPlaceHolder}
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="contact list">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            <Contacts
              contacts={contacts as ChatRoomReadyType[]}
              onItemClick={handleContactClick}
            />
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            <Contacts
              contacts={contacts as ChatRoomReadyType[]}
              onItemClick={handleContactClick}
            />
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar}></div>
        {activeRoom ? (
          <ChatBoardBody
            contact={activeRoom}
            user={user}
            onSendMessage={handleSendMessage}
            onLoadMore={handleLoadMore}
          />
        ) : (
          <React.Fragment>
            <div className={classes.emptyWrapper}>
              <EmptyBoard width="300" height="300"></EmptyBoard>
            </div>
          </React.Fragment>
        )}
      </main>
    </div>
  );
}

export default ChatBoard;
