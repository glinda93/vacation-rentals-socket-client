import React, { useCallback, useEffect, useContext } from 'react';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { Badge } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
// import { SocketContext } from '../../hooks/useSocket';
// import { SocketEvents } from '../../types/socket-event';
import { GlobalStateType } from '../../types/global';
import { getContactName } from '../../helpers/contact';
import Avatar from '../Avatar';
import { ChatRoomReadyType } from '../../redux/types/ChatType';
import Loader from '../Chatbox/Loader';
import useAudio from '../../hooks/useAudio';
import { useDispatch, useSelector } from 'react-redux';
import { ChatActionEnum } from '../../redux/actions/ChatAction';
import ContractIcon from './ContactIcon';
import useContact from "../../hooks/useContact";
import InfiniteScroll from 'react-infinite-scroll-component';
import LoadMoreLoader from '../LoadMoreLoader';
import { SocketContext } from '../../hooks/useSocket';
import { SocketEvents } from '../../types/socket-event';
import { ContactActionEnum } from '../../redux/actions/ContactAction';

const useStyles = makeStyles((theme) => ({
  navContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
  contactListWrapper: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    overflow: 'auto',
    '&::-webkit-scrollbar': {
      width: '0.4em',
    },
    '&::-webkit-scrollbar-track': {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
      webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,.1)',
    },
  },
  toolbar: {
    ...theme.mixins.toolbar,
    // flex: "1",
    display: 'flex',
    justifyContent: 'center',
  },
  emptyContactWrapper: {
    minHeight: '300px',
    display: 'flex',
    justifyContent: 'center',
  },
  emptyContact: {
    margin: '10px',
    alignSelf: 'center',
  },
  contact_name: {
    alignSelf: 'center',
  },
  unread_contact: {
    fontWeight: 700,
  },
  loader: {
    alignSelf: 'center',
  },
  loaderContainer: {
    display: 'flex',
    height: '100%',
  },
  contactLoaderContainer: {
    height: '100%',
    position: "relative"
  },
  contactLoader: {
    position: "absolute",
    bottom: 0
  },
  infiniteScroll: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
}));
const Contacts = ({
  contacts,
  onItemClick,
}: {
  contacts: ChatRoomReadyType[];
  onItemClick: (contact_name: ChatRoomReadyType) => void;
}) => {
  const classes = useStyles();
  const socket = useContext(SocketContext);
  const { getContacts } = useContact();
  const dispatch = useDispatch();
  
  useEffect(() => {
    if (!socket) return;

    socket.emit(SocketEvents.GET_CONTACTS, { });
    dispatch({
      type: ContactActionEnum.CONTACT_INIT,
    });
  }, [socket, dispatch]);

  // const selfContacts = contacts.filter(
  //   (contact) => contact.participants.length === 1
  // );

  const user = useSelector((state: GlobalStateType) => state.user);
  const contactStatus = useSelector((state: GlobalStateType) => state.contact);
  const { play } = useAudio(
    'https://blueskyapi.vacation.rentals/message-notification.mp3'
  );
  const isOtherMsg = useCallback((contact: ChatRoomReadyType): boolean => {
    const msgs = contact.messageConnection.edges;
    const lastMsg = msgs[msgs.length - 1];
    return lastMsg.userId !== user.id
  }, [user]);
  
  useEffect(() => {
    for (const contact of contacts) {
      if (
        !contact.loading &&
        !contact.active &&
        contact.newMsgArrived &&
        isOtherMsg(contact)
      ) {
        play();
        dispatch({
          type: ChatActionEnum.CLEAR_NEW_ARRIVED,
          payload: contact,
        });
      }
    }
  }, [contacts, dispatch, play, isOtherMsg]);
  
  const sortedContacts = contacts
    .filter((contact) => contact.participants.length !== 1)
    .sort((contactA, contactB) =>
      contactA.newMsgArrived === contactB.newMsgArrived
        ? 0
        : contactA.newMsgArrived
        ? 1
        : -1
    );
  
  return (
    <div className={classes.navContainer}>
      <div className={classes.toolbar}>
        <ContractIcon width="30" height="30"></ContractIcon>
        {/* <Typography
          align="center"
          variant="caption"
          display="block"
          gutterBottom
        >
          Contacts
        </Typography> */}
      </div>
      <Divider />

      {contacts.length === 0 ? (
        <React.Fragment>
          {contactStatus.contactArrived ? (
            <div className={classes.emptyContactWrapper}>
              <div className={classes.emptyContact}>Your Contact is empty</div>
            </div>
          ) : (
            <Loader
              containerProps={{ className: classes.loaderContainer }}
              loaderProps={{ className: classes.loader }}
            ></Loader>
          )}
        </React.Fragment>
      ) : (
        <div id="contact-list" className={classes.contactListWrapper}>
          <List>
            <InfiniteScroll
              dataLength={sortedContacts.length}
              next={getContacts}
              hasMore={contactStatus.hasNext}
              loader={<LoadMoreLoader/>}
              className={classes.infiniteScroll}
              scrollableTarget="contact-list"
            >
              {sortedContacts.map((contact) => (
                <ListItem
                  button
                  key={contact.name}
                  alignItems="flex-start"
                  selected={contact.active}
                  onClick={() => onItemClick(contact)}
                >
                  <ListItemAvatar>
                    {contact.unreadMessageCount > 0 ? (
                      <Badge
                        badgeContent={contact.unreadMessageCount}
                        color="primary"
                      >
                        <Avatar user={contact.participants[0]}></Avatar>
                      </Badge>
                    ) : (
                      <Avatar user={contact.participants[0]}></Avatar>
                    )}
                  </ListItemAvatar>
                  <ListItemText
                    classes={
                      contact.unreadMessageCount > 0
                        ? { primary: classes.unread_contact }
                        : {}
                    }
                    className={classes.contact_name}
                    primary={getContactName(contact)}
                  />
                </ListItem>
              ))}
            </InfiniteScroll>
          </List>
        </div>
      )}
    </div>
  );
};

export default Contacts;