import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
// import useInterval from '../hooks/useInterval';
import { DateActionType } from '../redux/actions/DateAction';

const UPDATE_INTERVAL = 60 * 1000;

const Interval = () => {
  // const { set } = useInterval();
  const dispatch = useDispatch();
  useEffect(() => {
    const handle = setInterval(
      dispatch(DateActionType.UPDATE),
      UPDATE_INTERVAL
    );
    return () => {
      clearInterval(handle);
    };
  }, [dispatch]);

  return null;
};

export default Interval;
