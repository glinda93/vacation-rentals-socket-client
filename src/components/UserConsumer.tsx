import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loadUserFromStorage } from '../helpers/user';
import { UserActionEnum } from '../redux/actions/UserAction';
import { UserType } from '../redux/types/UserType';
import { GlobalStateType } from '../types/global';

const UserConsumer = ({ localStorageKey }: { localStorageKey: string }) => {
  const user = useSelector((state: GlobalStateType) => state.user);
  const dispatch = useDispatch();
  useEffect(() => {
    const savedUser = loadUserFromStorage(localStorageKey) as UserType;
    if (savedUser && savedUser.name) {
      dispatch({
        type: UserActionEnum.SET_NAME,
        payload: savedUser.name,
      });
    }
  }, [dispatch, localStorageKey]);
  useEffect(() => {
    if (!user.id) {
      return;
    }
    if (user.id.startsWith('guest:')) {
      window.localStorage.setItem(localStorageKey, JSON.stringify(user));
    } else if (user.id.startsWith('user:')) {
      window.localStorage.removeItem(localStorageKey);
    }
  }, [user, localStorageKey]);
  return null;
};

export default UserConsumer;
