import React, { useCallback, useEffect, useRef, useState } from 'react';
import Root from './components/Root';
import logo from './logo.svg';
import './App.css';
import { ComponentType } from './components/ChatComponent';

const SOCKET_URL = process.env['REACT_APP_SOCKET_HOST'] as string;
const LOCAL_STORAGE_KEY = process.env[
  'REACT_APP_LOCAL_STORAGE_GUEST_KEY'
] as string;
const COOKIE_TOKEN_KEY = process.env['REACT_APP_COOKIE_TOKEN_KEY'] as string;

function App() {
  const rootRef = useRef<any>();
  const [participantId, setParticipantId] = useState('');
  const [layout, setLayout] = useState('dashboard');

  const startChatting = useCallback(() => {
    if (rootRef.current && layout === ComponentType.WIDGET) {
      rootRef.current.startChat(participantId);
    }
  }, [participantId, layout]);

  useEffect(() => {
    if (rootRef.current && layout === ComponentType.WIDGET) {
      rootRef.current.startLastChats();
    }
  }, [layout]);

  return (
    <div className="App">
      <select value={layout} onChange={(e) => setLayout(e.target.value)}>
        <option value="widget">Widget</option>
        <option value="dashboard">Dashboard</option>
      </select>
      {layout === 'widget' && (
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <input
            type="text"
            placeholder="Participant User ID"
            value={participantId}
            onChange={(e) => setParticipantId(e.target.value)}
          />
          <button className="App-link" onClick={startChatting}>
            Start Chatting
          </button>
        </header>
      )}
      <Root
        socketUrl={SOCKET_URL}
        guestStorageKey={LOCAL_STORAGE_KEY}
        tokenCookieKey={COOKIE_TOKEN_KEY}
        layout={layout}
        ref={rootRef}
      />
    </div>
  );
}

export default App;
