import { ChatRoomType, ChatRoomReadyType } from '../redux/types/ChatType';
import { UserType } from '../redux/types/UserType';
import { ContactStatusType } from '../redux/types/ContactType';

export type DynamicObjectType = {
  [key: string]: unknown;
};

export type GlobalStateType = {
  chat: ChatRoomType[];
  user: UserType;
  contact: ContactStatusType;
};
