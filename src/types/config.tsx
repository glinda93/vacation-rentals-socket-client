export type ConfigType = {
  socketUrl: string;
  guestStorageKey: string;
  tokenCookieKey: string;
  layout: string;
};
