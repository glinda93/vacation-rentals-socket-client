import { UserType } from '../redux/types/UserType';

export enum UserTypeEnum {
  USER = 'user',
  GUEST = 'guest',
}

export const loadUserFromStorage = (storageKey: string) => {
  const json = localStorage.getItem(storageKey) as string;
  try {
    return JSON.parse(json);
  } catch (e) {
    console.error(e);
    return undefined;
  }
};

export const getUserType = (user: UserType) => {
  if (user && user.id && user.id.startsWith('user:')) {
    return UserTypeEnum.USER;
  }
  return UserTypeEnum.GUEST;
};

export const isGuest = (user: UserType): boolean => {
  if (user && user.id && user.id.startsWith('user:')) {
    return false;
  }
  return true;
};

export const isAnonymous = (user: UserType): boolean => {
  return isGuest(user) && !user.name;
};
