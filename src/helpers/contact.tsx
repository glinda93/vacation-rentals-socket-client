import {
  // ChatRoomLoadingType,
  ChatRoomReadyType,
  // ChatRoomType,
} from '../redux/types/ChatType';
// import { isEqualIgnoringOrder } from './array';

export const getContactName = (
    contact: ChatRoomReadyType
) => {
    return contact.roomData.name || (contact.participants[0] && contact.participants[0].name) || contact.name;
}