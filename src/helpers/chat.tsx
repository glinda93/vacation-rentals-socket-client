import { MessageType } from '../redux/types/ChatType';
import { UserType } from '../redux/types/UserType';

export enum MessageTypeEnum {
  SENT = 'SENT',
  RECEIVED = 'RECEIVED',
}

export const getMessageType = (message: MessageType, user: UserType) => {
  if (message.userId === user.id) {
    return MessageTypeEnum.SENT;
  }
  return MessageTypeEnum.RECEIVED;
};
