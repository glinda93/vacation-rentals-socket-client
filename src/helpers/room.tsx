import {
  ChatRoomLoadingType,
  ChatRoomReadyType,
  ChatRoomType,
} from '../redux/types/ChatType';
import { isEqualIgnoringOrder } from './array';

export const isRoomExisting = (
  chats: ChatRoomType[],
  participantIds: string[]
) => {
  const loadingChats = chats.filter(({ loading }) => loading);
  if (
    loadingChats.findIndex((chat) => {
      return participantIds.includes(
        (chat as ChatRoomLoadingType).otherPartyId
      );
    }) > -1
  ) {
    return true;
  }
  const readyChats = chats.filter(({ loading }) => !loading);
  if (
    readyChats.findIndex((chat) => {
      return isEqualIgnoringOrder(
        participantIds,
        (chat as ChatRoomReadyType).participants.map(({ id }) => id)
      );
    }) > -1
  ) {
    return true;
  }
  return false;
};

export const getChatRoomIdxByName = (
  chatRooms: ChatRoomType[],
  roomName: string
): number => {
  return chatRooms
    .filter(({ loading }) => !loading)
    .findIndex((chatRoom) => {
      return (chatRoom as ChatRoomReadyType).name === roomName;
    });
};

export const getChatRoomByName = (
  chatRooms: ChatRoomType[],
  roomName: string
): ChatRoomReadyType | null => {
  const index = getChatRoomIdxByName(chatRooms, roomName);
  if (index === -1) {
    return null;
  }
  return chatRooms[index] as ChatRoomReadyType;
};
