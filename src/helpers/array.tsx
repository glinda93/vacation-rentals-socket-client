import * as lodash from 'lodash';

export const isEqualIgnoringOrder = (
  arr1: Array<unknown>,
  arr2: Array<unknown>
): boolean => {
  return lodash.isEqual(arr1.sort(), arr2.sort());
};
