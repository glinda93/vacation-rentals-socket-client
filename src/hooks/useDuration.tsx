import { useContext } from 'react';
import moment from 'moment';
import { TickContext } from './useTick';

const getCurrentDate = () => new Date();

const useDuration = (date: Date) => {
  const tick = useContext(TickContext);
  if (!tick) {
    return null;
  }
  const diff = getCurrentDate().getTime() - date.getTime();
  if (diff >= 0 && diff < 3600 * 1000) {
    const humanized = moment.duration(diff).humanize();
    if (humanized === 'a few seconds') {
      return 'just now';
    }
    return `${humanized} ago`;
  }
  if (diff >= 0 && diff < 3600 * 1000 * 24) {
    return moment(date).format('hh:mm');
  }
  return moment(date).format('M/D hh:mm');
};

export default useDuration;
