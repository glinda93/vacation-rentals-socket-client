import { createContext, useMemo } from 'react';
import SocketIO from 'socket.io-client';
import * as LocalStorage from './useLocalStorage';
import * as Cookie from './useCookie';
import { ParticipantType } from '../types/participant';

const getSocket = (
  socketUrl: string,
  guestStorageKey: string,
  tokenCookieKey: string
) => {
  const guest = LocalStorage.getItem(guestStorageKey);
  const token = Cookie.getItem(tokenCookieKey);
  if (token) {
    return SocketIO.connect(socketUrl, {
      query: token,
    });
  }
  if (guest && (guest as ParticipantType).id) {
    return SocketIO.connect(socketUrl, {
      query: {
        guestId: (guest as ParticipantType).id,
      },
    });
  }
  return SocketIO.connect(socketUrl);
};

const useSocket = (
  socketUrl: string,
  guestStorageKey: string,
  tokenCookieKey: string
) => {
  return useMemo(() => getSocket(socketUrl, guestStorageKey, tokenCookieKey), [
    socketUrl,
    guestStorageKey,
    tokenCookieKey,
  ]);
};

export const SocketContext = createContext<SocketIOClient.Socket | null>(null);

export default useSocket;
