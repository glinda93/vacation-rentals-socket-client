import { useCallback, useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getChatRoomByName, isRoomExisting } from '../helpers/room';
import { getUserType, UserTypeEnum } from '../helpers/user';
import { ChatActionEnum } from '../redux/actions/ChatAction';
import { ChatRoomReadyType } from '../redux/types/ChatType';
import { GlobalStateType } from '../types/global';
import { SocketEvents } from '../types/socket-event';
import { SocketContext } from './useSocket';
import * as LocalStorage from '../hooks/useLocalStorage';

const PREVIOUS_ROOMS_STORAGE_KEY = 'vr-previous-chat-rooms';

const getPreviousRoomNames = () => {
  return LocalStorage.getItem(PREVIOUS_ROOMS_STORAGE_KEY, []) as string[];
};

const useChat = (saveLastChats = false) => {
  const [lastRoomNames] = useState(getPreviousRoomNames());
  const dispatch = useDispatch();
  const socket = useContext(SocketContext);
  const user = useSelector((state: GlobalStateType) => state.user);
  const chats = useSelector((state: GlobalStateType) => state.chat);

  const start = useCallback(
    (otherPartyId: string) => {
      if (!socket) {
        return;
      }
      if (chats && isRoomExisting(chats, [user.id, otherPartyId])) {
        console.warn('Chat already started', [user.id, otherPartyId]);
        return;
      }
      dispatch({
        type: SocketEvents.INIT_CHAT,
        payload: { otherPartyId },
      });
      const userType = getUserType(user);
      if (userType === UserTypeEnum.USER) {

        socket.emit(SocketEvents.INIT_CHAT, {
          otherPartyId,
        });
      } else {

        socket.emit(SocketEvents.INIT_CHAT, {
          otherPartyId,
          guest: {
            name: user.name,
          },
        });
      }
    },
    [socket, user, dispatch, chats]
  );

  const sendMessage = useCallback(
    (roomName: string, message: string) => {
      if (!socket) {
        console.warn('socket has gone away');
        return;
      }
      const chat = getChatRoomByName(chats, roomName);
      if (!chat) {
        console.warn('Cannot find chat with name: ' + roomName, chats);
        return;
      }
      socket.emit(SocketEvents.SEND_MESSAGE, { roomName: chat.name, message });
    },
    [chats, socket]
  );

  const getMessageConnection = useCallback(
    (roomName: string, cursor: string | number | null) => {
      if (!socket) {
        return;
      }
      const chat = getChatRoomByName(chats, roomName);
      if (!chat) {
        return;
      }

      socket.emit(SocketEvents.GET_MESSAGE_CONNECTION, { roomName, cursor });
    },
    [chats, socket]
  );

  const getRoomData = useCallback(
    (roomName: string) => {
      if (!socket) {
        return;
      }
      
      socket.emit(SocketEvents.GET_ROOM_DATA, { roomName });
    },
    [socket]
  );

  const setActive = useCallback(
    (roomName: string) => {
      if (!socket) {
        return;
      }
      const chat = getChatRoomByName(chats, roomName);
      if (chat && chat.active && !chat.unreadMessageCount) {
        return;
      }
      dispatch({
        type: SocketEvents.SET_ACTIVE_ROOM,
        payload: {
          roomName,
        },
      });

      socket.emit(SocketEvents.SET_ACTIVE_ROOM, roomName);
    },
    [socket, dispatch, chats]
  );

  const close = useCallback(
    (index: number) => {
      if (!socket) {
        return;
      }
      dispatch({
        type: ChatActionEnum.CLOSE,
        payload: {
          index,
        },
      });

      socket.emit(SocketEvents.LEAVE_CHAT, index);
    },
    [socket, dispatch]
  );

  useEffect(() => {
    if (saveLastChats) {
        const readyRooms = chats.filter(
          ({ loading }) => !loading
        ) as ChatRoomReadyType[];
        const roomNames = readyRooms.map(({ name }) => name);
        LocalStorage.setItem(PREVIOUS_ROOMS_STORAGE_KEY, roomNames);
    } else {
        LocalStorage.setItem(PREVIOUS_ROOMS_STORAGE_KEY, []);
    }
  }, [chats, saveLastChats]);

  return {
    start,
    close,
    setActive,
    sendMessage,
    getMessageConnection,
    getRoomData,
    lastRoomNames,
  };
};

export default useChat;
