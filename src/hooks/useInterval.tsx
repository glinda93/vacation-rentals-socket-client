import { useCallback, useEffect, useState } from 'react';

const useInterval = () => {
  const [handle, setHandle] = useState<NodeJS.Timeout | null>(null);
  const set = useCallback((callback, delay) => {
    setHandle((oldHandle: NodeJS.Timeout | null) => {
      if (oldHandle) {
        clearInterval(oldHandle);
        return null;
      }
      return setInterval(callback, delay);
    });
  }, []);
  const clear = useCallback(() => {
    if (handle) {
      clearInterval(handle);
    }
  }, [handle]);
  useEffect(() => {
    return () => {
      clear();
    };
  }, [clear]);
  return { handle, clear, set };
};

export default useInterval;
