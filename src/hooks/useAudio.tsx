import { useCallback, useEffect, useState, useMemo } from 'react';

const playAudio = (audio: HTMLAudioElement) => {
  audio.play().catch(() => {
    const play = () => {
      audio
        .play()
        .catch((e) => {
          console.warn(e);
        })
        .finally(() => document.removeEventListener('mousemove', play));
    };
    document.addEventListener('mousemove', play);
  });
};

const useAudio = (url: string) => {
  const audio = useMemo(
    () => new Audio(url),
    [url]
  )
  const [playing, setPlaying] = useState(false);
  const play = useCallback(() => setPlaying(true), []);
  const pause = useCallback(() => setPlaying(false), []);
  const toggle = useCallback(() => setPlaying((oldPlaying) => !oldPlaying), []);
  useEffect(() => {
    const handleEnded = () => setPlaying(false);
    audio.addEventListener('ended', handleEnded);
    return () => {
      audio.removeEventListener('ended', handleEnded);
    };
  }, [audio]);

  useEffect(() => {
    playing ? playAudio(audio) : audio.pause();
  }, [playing, audio]);

  return {
    playing,
    play,
    pause,
    toggle,
    audio,
  };
};

export default useAudio;
