import { useCallback, useContext } from 'react';
import { SocketContext } from './useSocket';
import { SocketEvents } from '../types/socket-event';
import { useDispatch, useSelector } from 'react-redux';
import { GlobalStateType } from '../types/global';
import {ContactActionEnum} from '../redux/actions/ContactAction'

const useContact = () => {
  const socket = useContext(SocketContext);
  const dispatch = useDispatch();
  const { lastCursor } = useSelector((state: GlobalStateType) => state.contact);

  const getContacts = useCallback(() => {
    if (!socket) return;
    socket.emit(SocketEvents.GET_CONTACTS, { lastCursor: lastCursor });
    dispatch({
      type: ContactActionEnum.CONTACT_INIT,
    });
  }, [socket, dispatch, lastCursor]);
  const onSetContacts = useCallback(
    (lastCursor: number | string | null, hasNext: boolean) => {
      dispatch({
        type: ContactActionEnum.CONTACT_ARRIVED,
        payload: { lastCursor, hasNext },
      });
    },
    [dispatch]
  );

  return {
    getContacts,
    onSetContacts,
  };
}



export default useContact;