import { useState, useEffect, createContext } from 'react';

const INTERVAL = 60 * 1000;

const useTick = () => {
  const [date, setDate] = useState(new Date());
  useEffect(() => {
    const handle = setInterval(() => setDate(new Date()), INTERVAL);
    return () => {
      clearInterval(handle);
    };
  }, []);
  return date;
};

export const TickContext = createContext<Date>(new Date());

export default useTick;
