import { createContext } from 'react';
import { ConfigType } from '../types/config';

export const ConfigContext = createContext<ConfigType>({
  socketUrl: '',
  guestStorageKey: '',
  tokenCookieKey: '',
  layout: 'widget',
});
