import { useState, useEffect } from 'react';

export const getItem = (key: string, defaultValue: any = null): any => {
    const json = localStorage.getItem(key);
    if (!json) {
        return defaultValue||null;
    }
    try {
        return JSON.parse(json);
    } catch (e) {
        return defaultValue || null;
    }
}

export const setItem = (key: string, value: any): void => {
    const json = JSON.stringify(value);
    localStorage.setItem(key, json);
    return;
}

export const useLocalStorage = function <T>(
  key: string,
  defaultValue: T | null = null
) {
  const stored: string | null = localStorage.getItem(key);
  const initial = stored ? JSON.parse(stored) : defaultValue;
  const [value, setValue] = useState<T | null>(initial);

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);

  return [value, setValue];
};
